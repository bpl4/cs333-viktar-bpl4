#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <zlib.h>
#include <time.h>
#include <sys/sysmacros.h>
#include <pwd.h>
#include <errno.h>
#include <grp.h>
#include <utime.h>
#include <bsd/string.h>

#include "viktar.h"

#define BUF_SIZE 3000
//#define DEBUG

void printHelp(void);
void printPerms(mode_t m);
void printUserID(uid_t id);
void printGrID(gid_t id);

int main(int argc, char * argv[])
{
	int ifd = -1; // input file descriptor to read in files. Default will be from stdin.
	int ofd = -1; // output file descriptor for creating the archive. Default will be writing to stdout.
	int opt = 0;
	char FileName[VIKTAR_MAX_FILE_NAME_LEN] = {'\0'}; // the name of the file being created for -f
	ssize_t bytes_read = 0;
	ssize_t bytes_written = 0;
	size_t fileSize = 0;
	
	int fileOption = 0; // -f bool flag
	int extractOption = 0;
	int createOption = 0;
	int smallTableOption = 0;
	int bigTableOption = 0;
	int verbose = 0;
	unsigned char * buf = NULL;


	struct stat file_stat;
	struct tm *temp;
	struct utimbuf tmbuf;
	viktar_header_t myViktarHeader = {0};
	viktar_footer_t myViktarFooter = {0};
	uInt crc32_data;

	unsigned long totalFileSize = 0;

	off_t offset = 0;
	char outstr[100] = {'\0'};
	char modestr[11] = {'\0'};

	// processing the getopt
	while ((opt = getopt(argc, argv, OPTIONS)) != -1) // OPTIONS "xctTf:hv"
	{
		switch (opt)
		{
			case 'x': // extract
				createOption = 0;
				smallTableOption = 0;
				bigTableOption = 0;
				extractOption = 1;
				break;
			case 'c': // create a viktar archive
				createOption = 1;
				smallTableOption = 0;
				bigTableOption = 0;
				extractOption = 0;
				break;
			case 't': // short table of contents
				createOption = 0;
				smallTableOption = 1;
				extractOption = 0;
				bigTableOption = 0;
				break;
			case 'T': // long table of contents
				createOption = 0;
				smallTableOption = 0;
				bigTableOption = 1;
				extractOption = 0;
				break;

			// ONLY ONE OF THE OPTIONS ABOVE NEED TO BE CHOSEN OR WE DO NOTHING!

			case 'f': // give the file the name.	
				fileOption = 1;
				strncpy(FileName, optarg, VIKTAR_MAX_FILE_NAME_LEN);
				break;

			case 'h': // help
				printHelp();
				exit(EXIT_SUCCESS);
				break;
			case 'v': // verbose
				fprintf(stderr, "verbose level: 1\n");
				verbose = 1;
				break;
			default:
				fprintf(stderr, "oopsie - unrecognized command line option \"(null)\"\n");
				break;
		}
	}



	if (createOption) // -c option was enabled.
	{
		if (fileOption) // if -f used
		{
			// try to create the file. this should return -1 if a file already exists.
			ofd = open(FileName, O_WRONLY | O_CREAT | O_EXCL, S_IRUSR | S_IWUSR | S_IROTH | S_IRGRP);

			if (ofd == -1) // if file does not exist...
			{
				//close(ofd);
				ofd = open(FileName, O_WRONLY | O_TRUNC | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
				// the goal is to chmod only if the file does not exist yet.
			}

			else // change the file permissions if we created the file.
				chmod(FileName, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
		}

		else // otherwise, write/read to stdin and stdout.
			ofd = 1;

		// Creating the Archive
		if (verbose)
			fprintf(stderr, "creating archive file: \"%s\"\n", FileName);

		write(ofd, VIKTAR_FILE, strlen(VIKTAR_FILE)); // we have to write that $<viktar> thingy.
		if (optind < argc) // we are using optind to get the files given to archive.
		{
			for (int i = optind; i < argc; ++i) // iterating each file given.
			{
				// create our header
				if (-1 == lstat(argv[i], &file_stat))
				{
					perror("lstat failed!");
					exit(EXIT_FAILURE);
				}
				strncpy(myViktarHeader.viktar_name, argv[i], VIKTAR_MAX_FILE_NAME_LEN);
				myViktarHeader.st_size = file_stat.st_size;
				myViktarHeader.st_mode = file_stat.st_mode;
				myViktarHeader.st_uid = file_stat.st_uid;
				myViktarHeader.st_gid = file_stat.st_gid;
				myViktarHeader.st_atim = file_stat.st_atim;
				myViktarHeader.st_mtim = file_stat.st_mtim;


				// Write the header
				bytes_written = write(ofd, &(myViktarHeader), sizeof(viktar_header_t));
				if (bytes_written < 0)
				{
					perror("header write failed!");
					exit(EXIT_FAILURE);
				}
				totalFileSize += sizeof(myViktarHeader);

				// read the file that is gonna get archived
				ifd = open(argv[i], O_RDONLY, S_IRUSR | S_IRGRP | S_IROTH);
				fileSize = myViktarHeader.st_size;
				buf = malloc(fileSize * sizeof(unsigned char));
				bytes_read = read(ifd, buf, fileSize * sizeof(unsigned char));
				if (bytes_read < 0)
				{
					perror("file read failed!");
					exit(EXIT_FAILURE);
				}

				// write the file data
				bytes_written = write(ofd, buf, bytes_read);
				if (bytes_written < 0)
				{
					perror("file data write failed!");
					exit(EXIT_FAILURE);
				}
				totalFileSize += myViktarHeader.st_size;	

				if (buf != NULL)
				{
					free(buf);
					buf = NULL;
				}


				// create our footer and the crc data //////////////////////////////////////////////////////////
				ifd = open(argv[i], O_RDONLY, S_IRUSR | S_IRGRP | S_IROTH);
				crc32_data = crc32(0x0, Z_NULL, 0);
				buf = malloc(BUF_SIZE * sizeof(unsigned char));
				for( ; (bytes_read = read(ifd, buf, BUF_SIZE * sizeof(unsigned char))) > 0; ) 
				{
					crc32_data = crc32(crc32_data, buf, bytes_read);
				}


//				bytes_read = read(ifd, buf, BUF_SIZE);	
				if(bytes_read < 0)
				{
					perror("footer read failed!");
					exit(EXIT_FAILURE);
				}
				myViktarFooter.crc32_data = crc32_data;
				if (verbose)
				{
					fprintf(stderr, "\tadding file: %s to archive: %s\n", argv[i], FileName);
					fprintf(stderr, "\t crc value for data: 0x%08x\n", crc32_data);
				}
				//myViktarFooter.crc32_data = crc32_data;

				// write the footer
				bytes_written = write(ofd, &(myViktarFooter), sizeof(viktar_footer_t));
				if (bytes_written < 0)
				{
					perror("file data write failed!");
					exit(EXIT_FAILURE);
				}

				totalFileSize += sizeof(myViktarFooter);
				if (buf != NULL)
				{
					free(buf);
					buf = NULL;
				}
			}

			if (ifd != 0)
				close(ifd);
		}
	
	}

	// Small Table Option -t
	if (smallTableOption)
	{
		if (fileOption) // if -f used
		{

			fprintf(stderr, "reading archive file: \"%s\"\n", FileName);
			ifd = open(FileName, O_RDONLY);
			if (ifd < 0)
			{
				fprintf(stderr, "failed to open input archive file %s\n", FileName);
				fprintf(stderr, "exiting...\n");
				exit(EXIT_FAILURE);
			}


		}

		else // otherwise, write/read to stdin and stdout.
		{
			ifd = 0;
			strcpy(FileName, "stdin");
			fprintf(stderr, "reading archive from stdin\n");
		}


		/*
		if (verbose)
			printf("reading archive file: \"%s\"\n", FileName);
			*/

		buf = malloc((strlen(VIKTAR_FILE)) * sizeof(unsigned char));
		bytes_read = read(ifd, buf, (strlen(VIKTAR_FILE)) * sizeof(unsigned char));
		if (bytes_read < 0)
		{
			perror("read() for extract failed!");
			exit(EXIT_FAILURE);
		}

		if (strcmp((char *)buf, VIKTAR_FILE) != 0)
		{
			fprintf(stderr, "not a viktar file: \"%s\"\n", FileName); // IMPOSTER DETECTED
			fprintf(stderr, "\tthis is vik-terrible\n");
			fprintf(stderr, "\texiting...\n");
			if (buf)
			{
				free(buf);
				buf = NULL;
			}
			exit(EXIT_FAILURE);
		}

		if (buf)
		{
			free(buf);
			buf = NULL;
		}


		printf("Contents of viktar file: \"%s\"\n", FileName);

		// first, we need to lseek and go past the $<viktar> part.
		offset = strlen(VIKTAR_FILE);
		lseek(ifd, offset, SEEK_SET);

		// we will need to use lseek to iterate each header.
		while(read(ifd, &(myViktarHeader), sizeof(viktar_header_t)) > 0)
		{
			printf("\tfile name: %s\n", myViktarHeader.viktar_name);
			offset = myViktarHeader.st_size + sizeof(viktar_footer_t);
			lseek(ifd, offset, SEEK_CUR);
		}
		
	}

	if (bigTableOption) // option -T
	{
		if (fileOption) // if -f used
		{

			fprintf(stderr, "reading archive file: \"%s\"\n", FileName);
			ifd = open(FileName, O_RDONLY);
			if (ifd < 0)
			{
				perror("file open failed!");
				exit(EXIT_FAILURE);
			}


		}

		else // otherwise, write/read to stdin and stdout.
		{
			ifd = 0;
			strcpy(FileName, "stdin");
			fprintf(stderr, "reading archive from stdin\n");
		}

		// check the viktar tag. make sure its an actual viktar file
		buf = malloc((strlen(VIKTAR_FILE)) * sizeof(unsigned char));
		bytes_read = read(ifd, buf, (strlen(VIKTAR_FILE)) * sizeof(unsigned char));
		if (bytes_read < 0)
		{
			perror("read() for extract failed!");
			exit(EXIT_FAILURE);
		}

		if (strcmp((char *)buf, VIKTAR_FILE) != 0)
		{
			fprintf(stderr, "not a viktar file: \"%s\"\n", FileName); // IMPOSTER
			fprintf(stderr, "\tthis is vik-terrible\n");
			fprintf(stderr, "\texiting...\n");
			if (buf)
			{
				free(buf);
				buf = NULL;
			}
			exit(EXIT_FAILURE);
		}

		if (buf)
		{
			free(buf);
			buf = NULL;
		}

		printf("Contents of viktar file: \"%s\"\n", FileName);
		while(read(ifd, &(myViktarHeader), sizeof(viktar_header_t)) > 0) // first, read the header
		{
			// print header contents
			printf("\tfile name: %s\n", myViktarHeader.viktar_name);
			printf("\t\tmode:         ");
			strmode(myViktarHeader.st_mode, modestr); // using the bsd/string.h strmode function.
			printf("%s\n", modestr);
			//printPerms(myViktarHeader.st_mode); // basically my version of strmode
			printf("\t\tuser:         ");
			printUserID(myViktarHeader.st_uid);
			printf("\t\tgroup:        ");
			printGrID(myViktarHeader.st_gid);
			printf("\t\tsize:         %ju\n", myViktarHeader.st_size);

			temp = localtime(&myViktarHeader.st_mtim.tv_sec); // printing time
			if (temp == NULL)
			{
				perror("Unable to get local time");
				exit(EXIT_FAILURE);
			}

			if (strftime(outstr, sizeof(outstr), "%Y-%m-%d %H:%M:%S %Z", temp) == 0)
			{
				fprintf(stderr, "strftime call failed");
				exit(EXIT_FAILURE);
			}

			printf("\t\tmtime:        %s\n", outstr);

			temp = localtime(&myViktarHeader.st_atim.tv_sec);
			if (temp == NULL)
			{
				perror("Unable to get local time");
				exit(EXIT_FAILURE);
			}

			if (strftime(outstr, sizeof(outstr), "%Y-%m-%d %H:%M:%S %Z", temp) == 0)
			{
				fprintf(stderr, "strftime call failed");
				exit(EXIT_FAILURE);
			}

			printf("\t\tatime:        %s\n", outstr);

			offset = myViktarHeader.st_size;
			lseek(ifd, offset, SEEK_CUR); // go past the file data.

			// read the footer.
			bytes_read = read(ifd, &(myViktarFooter), sizeof(myViktarFooter));
			if (bytes_read < 0)
			{
				perror("Footer read failed!");
				exit(EXIT_FAILURE);
			}

			printf("\t\tcrc32 data:   0x%08x\n", myViktarFooter.crc32_data);

				
			//offset = sizeof(viktar_footer_t);
			//lseek(ifd, offset, SEEK_CUR);
		}

	}

	if (extractOption) // option -x
	{
		if (fileOption) // if -f used
		{
			ifd = open(FileName, O_RDONLY);
			if (ifd < 0)
			{
				fprintf(stderr, "failed to open input archive file %s\n", FileName);
				fprintf(stderr, "exiting...\n");
				exit(EXIT_FAILURE);
			}
			//printf("reading archive file: \"%s\"\n", FileName);
		}

		else // otherwise, write/read to stdin and stdout.
		{
			ifd = 0;
			strcpy(FileName, "stdin");
			fprintf(stderr, "reading archive from %s\n", FileName);
		}


		// check if the file to be opened has a .viktar	

		
		// First, get past the $<viktar>. But we also need to check it.
		buf = malloc((strlen(VIKTAR_FILE)) * sizeof(unsigned char));
		bytes_read = read(ifd, buf, (strlen(VIKTAR_FILE)) * sizeof(unsigned char));
		if (bytes_read < 0)
		{
			perror("read() for extract failed!");
			exit(EXIT_FAILURE);
		}

		if (strcmp((char *)buf, VIKTAR_FILE) != 0)
		{
			fprintf(stderr, "not a viktar file: \"%s\"\n", FileName);
			fprintf(stderr, "\tthis is vik-terrible\n");
			fprintf(stderr, "\texiting...\n");
			if (buf)
				free(buf);
			exit(EXIT_FAILURE);
		}

		if (buf)
			free(buf);

		if (optind < argc) // for extracting specified files.
		{
			for (int i = optind; i < argc; ++i)
			{
				// read the header
				bytes_read = read(ifd, &(myViktarHeader), sizeof(viktar_header_t));

				// create/extract the file
				ofd = open(argv[i], O_RDONLY | O_CREAT | O_TRUNC, myViktarHeader.st_mode);
			
				buf = malloc(myViktarHeader.st_size * sizeof(unsigned char));
				bytes_read = read(ifd, buf, myViktarHeader.st_size * sizeof(unsigned char));

				bytes_written = write(ofd, buf, bytes_read);
				if (buf)
					free(buf);
			
				// read and check crc data
				bytes_read = read(ifd, &(myViktarFooter), sizeof(viktar_footer_t));

				crc32_data = crc32(0x0, Z_NULL, 0);	
				buf = malloc(BUF_SIZE * sizeof(unsigned char));
				for( ; (bytes_read = read(ofd, buf, BUF_SIZE * sizeof(unsigned char))) > 0; ) 
				{
					crc32_data = crc32(crc32_data, buf, bytes_read); // 
				}

				if (crc32_data != myViktarFooter.crc32_data) // checking crc
				{
					fprintf(stderr, "*** CRC32 failure data: %s  in file: 0x%08x  extract: 0x%08x ***\n", myViktarHeader.viktar_name, myViktarFooter.crc32_data, crc32_data);
				}
			}
		}

		else // if no files given extract all
		{
			while(read(ifd, &(myViktarHeader), sizeof(viktar_header_t)) > 0)
			{
				fileSize = myViktarHeader.st_size;
				buf = malloc(fileSize * sizeof(unsigned char));
				bytes_read = read(ifd, buf, fileSize * sizeof(unsigned char)); // reading the file in the archive.

				// Extract the file (i.e. literally duplicate the file)
				ofd = open(myViktarHeader.viktar_name, O_RDWR | O_CREAT | O_TRUNC, myViktarHeader.st_mode);
				bytes_written = write(ofd, buf, bytes_read); // write to that file.
				if (bytes_written < 0)
				{
					perror("write() has failed!");
					exit(EXIT_FAILURE);
				}

				if (buf != NULL)
				{
					free(buf);
					buf = NULL;
				}


				bytes_read = read(ifd, &(myViktarFooter), sizeof(viktar_footer_t)); // read the footer
				if (bytes_read < 0)
				{
					perror("read() has failed!");
					exit(EXIT_FAILURE);
				}

				// After extracting, we need to check the CRC value of that extracted file.
				if (ofd != 1 && ofd != 2)
					close(ofd);

				ofd = open(myViktarHeader.viktar_name, O_RDONLY, myViktarHeader.st_mode);
				crc32_data = crc32(0x0, Z_NULL, 0);	
				buf = malloc(BUF_SIZE * sizeof(unsigned char));
				for( ; (bytes_read = read(ofd, buf, BUF_SIZE * sizeof(unsigned char))) > 0; ) 
				{
					crc32_data = crc32(crc32_data, buf, bytes_read); // 
				}

				if (crc32_data != myViktarFooter.crc32_data) // checking crc
				{
					fprintf(stderr, "*** CRC32 failure data: %s  in file: 0x%08x  extract: 0x%08x ***\n", myViktarHeader.viktar_name, myViktarFooter.crc32_data, crc32_data);
				}

				if (buf)
				{
					free(buf);
					buf = NULL;
				}

				// we also need to restore the file timestamps to match what was in the archive.
				//lseek(ofd, bytes_written * -1, SEEK_CURRENT); // going back to the beginning.
				tmbuf.actime = myViktarHeader.st_atim.tv_sec;
				tmbuf.modtime = myViktarHeader.st_mtim.tv_sec;
				utime(myViktarHeader.viktar_name, &tmbuf); // utime library found online and man pages.
				chmod(myViktarHeader.viktar_name, myViktarHeader.st_mode); // restore the file perms
			}
		}
	}

	if (buf != NULL)
	{
		free(buf);
		buf = NULL;
	}

	if (ifd != 0)
		close(ifd);
	if (ofd != 1 && ofd != 2)
		close(ofd);

	return EXIT_SUCCESS;
}

void printHelp(void)
{
	printf("help text\n");
	printf("        Options: xctTf:hv\n");
	printf("                -x              extract file/files from archive\n");
	printf("                -c              create an archive file\n");
	printf("                -t              display a short table of contents of the archive file\n");
	printf("                -T              display a long table of contents of the archive file\n");
	printf("                Only one of xctT can be specified\n");
	printf("                -f filename     use filename as the archive file\n");
	printf("                -v              give verbose diagnostic messages\n");
	printf("                -h              display this AMAZING help message\n");
}

// will be using strmode() instead.
void printPerms(mode_t m) // function to print the permissions of each files and their octal value
{
	// m is the st_mode in viktar header

	char perms[11] = {'\0'}; // 

	if (S_ISREG(m)) // finding the file type.
                perms[0] = '-';
        else if (S_ISDIR(m))
                perms[0] = 'd';
        else if (S_ISCHR(m))
                perms[0] = 'c';
        else if (S_ISBLK(m))
                perms[0] = 'b';
        else if(S_ISFIFO(m))
                perms[0] = 'p';
        else if (S_ISLNK(m))
                perms[0] = 'l';
        else if (S_ISSOCK(m))
                perms[0] = 's';

	// setting the bits and printing permissions.
	// This was from mystat.h
        (m & S_IRUSR) ? (perms[1] = 'r') : (perms[1] = '-'); // owner read
	(m & S_IWUSR) ? (perms[2] = 'w') : (perms[2] = '-'); // owner write

	if (m & S_IXUSR) // checking for the setuid and setgid bits
		(m & S_ISUID) ? (perms[3] = 's') : (perms[3] = 'x'); // owner execute (or those setuid/setgid bits)
	else
		(m & S_ISUID) ? (perms[3] = 'S') : (perms[3] = '-');


        (m & S_IRGRP) ? (perms[4] = 'r') : (perms[4] = '-'); // grp part
	(m & S_IWGRP) ? (perms[5] = 'w') : (perms[5] = '-');

	if (m & S_IXGRP) // checking for the setuid and setgid bits
		(m & S_ISGID) ? (perms[6] = 's') : (perms[6] = 'x');
	else
		(m & S_ISGID) ? (perms[6] = 'S') : (perms[6] = '-');

        (m & S_IROTH) ? (perms[7] = 'r') : (perms[7] = '-'); // oth part
	(m & S_IWOTH) ? (perms[8] = 'w') : (perms[8] = '-');
	(m & S_IXOTH) ? (perms[9] = 'x') : (perms[9] = '-');

	printf("%s", perms);

        printf("\n");
}

void printUserID(uid_t id)
{
        ssize_t bufsiz;
        char * buf = NULL;
        int s = 0;
        struct passwd pwd;
        struct passwd *result;

        // borrowed some portions of code from the man pages
        bufsiz = sysconf(_SC_GETPW_R_SIZE_MAX);

        if (bufsiz == -1)
                bufsiz = 16384;

        buf = malloc(bufsiz);
        if (buf == NULL)
        {
                perror("malloc");
                exit(EXIT_FAILURE);
        }

        s = getpwuid_r(id, &pwd, buf, bufsiz, &result);
        if (result == NULL)
        {
                if (s == 0)
                        printf("Not found\n");
                else
                {
                        errno = s;
                        perror("getpwnam_r");
                }
                exit(EXIT_FAILURE);
        }
        printf("%s\n", pwd.pw_name);

        free(buf);
}
void printGrID(gid_t id)
{
        int check = 0;
        struct group usr_grp;
        struct group *result;
        char * buf = NULL;
        ssize_t bufsiz;

        // portions of code were from the man pages.
        bufsiz = 16384;
        buf = malloc(bufsiz);
        if (buf == NULL)
        {
                perror("malloc");
                exit(EXIT_FAILURE);
        }

        check = getgrgid_r(id, &usr_grp, buf, bufsiz, &result);
        if (result == NULL)
        {
                if (check == 0)
                        printf("Not found\n");
                else
                {
                        perror("Something went wrong");
                }
                exit(EXIT_FAILURE);
        }
        printf("%s\n", usr_grp.gr_name);
        free(buf);
}

